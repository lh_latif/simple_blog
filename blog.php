<?php
require_once "./database/mysql.php";
require_once "./controllers/controller_artikel.php";

// testDatabase($conn);

// jika ada parameter judul
if (isset($_GET["judul"])) {
    $artikel = getArtikelBySlug($conn, $_GET["judul"]);
    require_once "./views/artikel-satu.php";
} elseif (isset($_POST["submit"])) {
    $pesan = addArtikel($conn, $_POST["judul"], $_POST["konten"], $_POST["slug"]);
    if ($pesan === true) {
        echo "berhasil";
        return;
    } else {
        echo "gagal $pesan";
    }
    return;

} elseif(isset($_GET["new"])) {
    require_once "./views/artikel-form.php";
} elseif (isset($_POST["update"])) {
    //  perintah update artikel
    if (updateArtikel($conn, $_POST["id"], $_POST["judul"], $_POST["konten"], $_POST["slug"]) === true) {
        echo 'Berhasil. <a href="http://localhost:8000/dashboard.php/?dashboard=true">Kembali</a>';
        header("Location : http://localhost:8000/blog.php");
    } else {
        // echo "ada Error";
        echo mysqli_error($conn);
    }

} else if (isset($_POST["delete"])) {
    // perintah hapus artikel
    if ($pesan = deleteArtikel($conn, $_POST["id"])) {
        echo 'Berhasil. <a href="http://localhost:8000/dashboard.php/?dashboard=true">Kembali</a>';
        header("Location : http://localhost:8000/dashboard.php/?dashboard=true");
    } else {
        echo "$pesan";
    }

} else if (isset($_GET["edit"]) && isset($_GET["id"])) {
    // mengambil data satu artikel terlebih dahulu menggunakan controller
    $artikel = getArtikel($conn, $_GET["id"]);
    // var_dump($artikel);
    require_once "./views/artikel-form.php";
    // lalu menampilkan views form untuk edit artikel

} else if (isset($_GET["dashboard"]) && $_GET["dashboard"] === "true") {
    // mengambil semua data artikel
    $semua_artikel = getAllArtikel($conn);
    // lalu menampilkan dashboard / control panel untuk edit dan delete
    require_once "./views/artikel-dashboard.php";
}

else {
    $data_database = getAllArtikel($conn);
    require "./views/artikel-index.php";
}

mysqli_close($conn);
?>
