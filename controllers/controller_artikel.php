<?php

// expression adalah function atau method yang mereturn/mengembalikan value
// sedangkan procedure adalah function atau method yang tidak mereturn value

function getArtikel($conn, $id) {

    if ($result = mysqli_query($conn, "SELECT * FROM artikel WHERE id=$id")) {
        return mysqli_fetch_assoc($result);
    } else {
        return false;
    }
}

function getArtikelBySlug($conn, $slug) {
    if ($result = mysqli_query($conn, "SELECT * FROM artikel WHERE slug='$slug'")) {
        return mysqli_fetch_assoc($result);
    }
}

function getAllArtikel($conn) {
    // mocking
    // ngambil semua data di database
    if ($result = mysqli_query($conn, "SELECT * FROM artikel")){

        $artikel = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $artikel[] = $row;
        }
        mysqli_free_result($result);
        return $artikel;
    }
}

function addArtikel($conn, $judul, $konten, $slug) {
    if ( $stmt = mysqli_prepare(
            $conn,
            "INSERT INTO artikel(judul, konten, slug) VALUES (?, ?, ?)")
    ){
        mysqli_stmt_bind_param($stmt, 'sss', $judul, $konten, $slug);

        return (mysqli_stmt_execute($stmt))?: mysqli_stmt_error($stmt);
        // ternary operator
    }
}

function updateArtikel($conn, $id, $judul, $konten, $slug) {
    $stmt = mysqli_prepare($conn, "UPDATE artikel SET judul=?, konten=?, slug=? WHERE id=?");
    if ($stmt) {
        mysqli_stmt_bind_param($stmt, 'sssi', $judul, $konten, $slug, $id);

        // menulis if untuk expression tanpa ternary sangatlah panjang dan tidak efisien
        // $execute = mysqli_stmt_execute($stmt);
        // if ($execute) {
        //     return $execute;
        // } else {
        //     return mysqli_stmt_error($stmt);
        // }

        // menggunakan ternary untuk melakukan operasi if untuk expression
        // var_dump($stmt);
        return (mysqli_stmt_execute($stmt))?: mysqli_stmt_error($stmt);
    } else {
        return $stmt;
    }
}

function deleteArtikel($conn, $id) {
    $stmt = mysqli_prepare($conn, "DELETE FROM artikel WHERE id=?");
    if ($stmt) {
        mysqli_stmt_bind_param($stmt, "i", $id);

        return (mysqli_stmt_execute($stmt))?: mysqli_stmt_error($stmt);
    } else {
        return "Mysqli Error";
    }
}

?>
