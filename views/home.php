<?php require_once "./views/layouts/header.php" ?>

  <div class="artikel">
    <h3 class="artikel-title">Assalamu'alaikum</h3>
    <p class="artikel-konten">Selamat datang di blog sederhana ini, saya Latif Hidayatullah saat ini seorang programmer.
        Kamu bisa panggi saya Latif atau Dayat. Saya membuat blog ini dengan PHP sebagai bahasa
        Server-side. Tahu nggak yang buat PHP itu namanya Lasmus Lerdorf. Dia membuat PHP di tahun
        1995 yang saat itu PHP jauh berbeda dengan PHP jaman sekarang.
    </p>
  </div>

<?php require_once "./views/layouts/footer.php" ?>
