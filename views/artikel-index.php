<?php require_once "./views/layouts/header.php" ?>

  <div class="artikel">
    <h3 class="artikel-title">Artikel</h3>
    <p class="artikel-konten">
        Ini adalah halaman untuk di isi dengan artikel.
        nantinya akan ada konten yang dinamis yang akan di generate oleh PHP
        di server nantinya
    </p>
    <ul>
    <?php foreach($data_database as $row): ?>
        <li>
            <a href="<?php echo "/blog.php/?judul=".$row['slug'] ?>">
                <?= $row["judul"] ?>
            </a>
        </li>
    <?php endforeach ?>
    </ul>

  </div>

<?php require_once "./views/layouts/footer.php" ?>
