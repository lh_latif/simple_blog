<?php require_once "./views/layouts/header.php" ?>

  <div class="artikel">
    <h3 class="artikel-title"><?= $artikel["judul"] ?></h3>
    <p class="artikel-konten"><?= $artikel["konten"] ?></p>
  </div>

<?php require_once "./views/layouts/footer.php" ?>
