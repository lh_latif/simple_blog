<?php
require_once "./views/layouts/header.php";
?>
<!-- <?= $artikel["judul"] ?> -->

<?php if(!isset($artikel)):?>
    <!-- <h1>True</h1> -->
    <div class="artikel">
        <form action="/blog.php" method="post">
            <div>
                <label for="judul">Judul</label>
                <input type="text" name="judul">
            </div>
            <div>
                <label for="slug">Slug</label>
                <input type="text" name="slug">
            </div>
            <div>
                <label for="konten">Konten</label>
                <textarea class="textarea" type="text" name="konten" rows="10"></textarea>
            </div>



            <input type="submit" value="new post" name="submit">
        </form>
    </div>
<?php else: ?>
    <!-- <h2>False</h2> -->
    <div class="artikel">
        <form action="/dashboard.php" method="post">
            <div>
                <label for="judul">Judul</label>
                <input type="text" name="judul" value="<?= $artikel["judul"] ?>" >
            </div>
            <div>
                <label for="slug">Slug</label>
                <input type="text" name="slug" value="<?= $artikel["slug"] ?>" >
            </div>
            <div>
                <label for="konten">Konten</label>
                <textarea class="textarea" type="text" name="konten" rows="10">
                    <?php echo $artikel["konten"] ?>
                </textarea>
            </div>
            <input type="hidden" name="id" value="<?= $artikel["id"] ?>">

            <input type="submit" value="Update Artikel" name="update">
        </form>
    </div>
<?php endif; ?>

<?php
require_once "./views/layouts/footer.php";
?>
