<?php require_once "./views/layouts/header.php" ?>
<div class="artikel">
  <table>
      <thead>
          <tr>
              <th>Judul</th>
              <th>Slug</th>
              <th>Aksi</th>
          </tr>
      </thead>
      <tbody>
          <?php foreach($semua_artikel as $artikel): ?>
          <tr>
              <td><?= $artikel["judul"] ?></td>
              <td><?= $artikel["slug"] ?></td>
              <td>
                  <button>
                      <a href="/dashboard.php/?edit=true&id=<?= $artikel["id"] ?>">Edit</a>
                  </button>
                  <form action="/dashboard.php" method="post">
                      <input type="hidden" value="<?= $artikel["id"] ?>" name="id">
                      <input type="submit" value="Hapus" name="delete">
                  </form>
              </td>
          </tr>
          <?php endforeach ?>
      </tbody>
  </table>
</div>
<?php require_once "./views/layouts/footer.php" ?>
