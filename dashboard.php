<?php
require_once "./database/mysql.php";
require_once "./controllers/controller_artikel.php";

if (isset($_POST["update"])) {
    //  perintah update artikel
    if (updateArtikel($conn, $_POST["id"], $_POST["judul"], $_POST["konten"], $_POST["slug"]) === true) {
        echo 'Berhasil. <a href="http://localhost:8000/dashboard.php/?dashboard=true">Kembali</a>';
        header("Location : http://localhost:8000/blog.php");
    } else {
        // echo "ada Error";
        echo mysqli_error($conn);
    }

} else if (isset($_POST["delete"])) {
    // perintah hapus artikel
    if ($pesan = deleteArtikel($conn, $_POST["id"])) {
        echo 'Berhasil. <a href="http://localhost:8000/dashboard.php/?dashboard=true">Kembali</a>';
        header("Location : http://localhost:8000/dashboard.php/?dashboard=true");
    } else {
        echo "$pesan";
    }

} else if (isset($_GET["edit"]) && isset($_GET["id"])) {
    // mengambil data satu artikel terlebih dahulu menggunakan controller
    $artikel = getArtikel($conn, $_GET["id"]);
    // var_dump($artikel);
    require_once "./views/artikel-form.php";
    // lalu menampilkan views form untuk edit artikel

} else if ($_GET["dashboard"] === "true") {
    // mengambil semua data artikel
    $semua_artikel = getAllArtikel($conn);
    // lalu menampilkan dashboard / control panel untuk edit dan delete
    require_once "./views/artikel-dashboard.php";
}

mysqli_close($conn);
